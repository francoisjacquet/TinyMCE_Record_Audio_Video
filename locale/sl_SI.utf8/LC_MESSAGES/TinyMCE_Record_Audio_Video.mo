��            )   �      �     �     �     �     �     �            H   -  H   v  P   �             T   /  ?   �  -   �     �       Q   "     t     �     �     �  %   �  9   �  ]   "  f   �  �   �  	   y    �     �	     �	     �	     �	     �	     
  #   
  ?   ;
  E   {
  Q   �
          '  S   7  G   �  0   �             c   =     �     �  *   �     �       =   !  Q   _  w   �  �   )  
   �                                                                     	                                
                                     Attach Recording as Annotation Audio annotation Insecure connection! Insert audio recording Insert video recording Record Again Recording failed, try again Something appears to have gone wrong, it seems nothing has been recorded Something is preventing the browser from accessing the webcam/microphone Something strange happened which prevented the webcam/microphone from being used Start Recording Stop Recording The current webcam/microphone can not produce a stream with the required constraints The user must allow the browser access to the webcam/microphone There is no input device connected or enabled Time Limit (seconds) TinyMCE Record Audio Video Tried to get stream from the webcam/microphone, but no constraints were specified Upload aborted: Upload failed: Upload failed: file too large Video annotation What should the annotation appear as? You have attained the maximum size limit for file uploads Your browser does not support recording over an insecure connection and must close the plugin Your browser might not allow this plugin to work unless it is used either over HTTPS or from localhost Your browser offers limited or no support for WebRTC technologies yet, and cannot be used with this plugin. Please switch or upgrade your browser completed Project-Id-Version: TinyMCE Record Audio Video plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:23+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Priložite posnetek kot opombo Zvočna opomba Povezava ni varna! Vstavite zvočni posnetek Vstavite video posnetek Ponovno snemaj Snemanje ni uspelo, poskusite znova Zdi se, da je šlo nekaj narobe, zdi se, da ni nič zabeleženo Nekaj ​​brskalniku preprečuje dostop do spletne kamere/mikrofona Zgodilo se je nekaj čudnega, kar je preprečilo uporabo spletne kamere/mikrofona Prični z snemanjem Ustavi snemanje Trenutna spletna kamera/mikrofon ne more ustvariti prenosa z zahtevanimi omejitvami Uporabnik mora brskalniku omogočiti dostop do spletne kamere/mikrofona Nobena vhodna naprava ni povezana ali omogočena Časovna omejitev (sekunde) TinyMCE snemanje avdio videa Poskušal sem pridobiti prenos iz spletne kamere/mikrofona, vendar niso bile podane nobene omejitve Nalaganje prekinjeno: Nalaganje ni uspelo: Nalaganje ni uspelo: datoteka je prevelika Video opomba Kako naj se prikaže opomba? Dosegli ste največjo omejitev velikosti za nalaganje datotek Vaš brskalnik ne podpira snemanja prek nevarne povezave in mora zapreti vtičnik Vaš brskalnik morda ne bo dovolil delovanja tega vtičnika, razen če se uporablja prek HTTPS ali lokalnega gostitelja Vaš brskalnik ponuja omejeno ali nobene podpore za tehnologije WebRTC in ga ni mogoče uporabljati s tem vtičnikom. Preklopite ali nadgradite brskalnik dokončano 